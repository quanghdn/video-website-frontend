import React, { Component } from 'react'
import { Link } from 'react-router'
import {App} from '../../views/index.js'
console.log(App);
class AppPage extends Component {
	render() {
		return (
			<App>
				{this.props.children}
			</App>
		)
	}
}

export default AppPage