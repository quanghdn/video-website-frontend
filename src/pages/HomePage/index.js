import React, {Component} from 'react'
import {observer} from 'mobx-react'
import load from '../../functions/loader';
import {Home} from '../../views/'
import {Tag as TagStore} from '../../stores/'
let HomePage = observer(() => {
		TagStore.getTagsList().then().catch(err=>console.log(err));
		if (TagStore.tags !== undefined) {
				return <Home tags={TagStore.tags}/>
		} else {
				return <div>Loading</div>
		}
})

export default HomePage;