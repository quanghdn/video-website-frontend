var Fetch = require('../functions/fetch');
var config = require('../config');

module.exports = {
    getCategories: function () {
        return Fetch.get('/server/category');
    },
    postCategory: function(data) {
    	return Fetch.post('/server/category', data);
    },
    getVideoInfo: function (videoID) {
    	return Fetch.get('https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatus&id=' + videoID + '&key=' + config.youtube.apiKey);
    },
    getChannelInfo: function (channelID) {
    	return Fetch.get('https://www.googleapis.com/youtube/v3/channels?part=contentDetails&id=' + channelID + '&key=' + config.youtube.apiKey);
    },
    getVideoFromPlaylist: function (playlistID, nextPageToken) {
    	var url = 'https://www.googleapis.com/youtube/v3/playlistItems?part=snippet&';
    	if (nextPageToken !== '') {
    		url += 'pageToken=' + nextPageToken + '&';
    	}
    	url += 'playlistId=' + playlistID + '&key=';
		url += config.youtube.apiKey;
		return Fetch.get(url);
    }
};