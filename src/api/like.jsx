var Fetch = require('../functions/fetch');

module.exports = {
    getVideoLike: function (userID, videoID) {
        return Fetch.get('/user/' + userID + '/like/' + videoID);
    }
};