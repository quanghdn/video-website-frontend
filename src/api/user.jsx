var Fetch = require('../functions/fetch');
let getUserInfo = function () {
		return Fetch.get('/server/user');
};
let getUserInfoFlow = async function () {
		try {
				let userInfo = await getUserInfo();
				console.log(userInfo);
				var status = userInfo.status;
				if (status === 0) {
						let auth = await Fetch.post('https://api.zenquiz.net/v1/remote_auth', {});
						console.log(auth);
						await Fetch.post('/server/session', auth);
						userInfo = await getUserInfo();
				}
				return userInfo;
		} catch (err) {
				console.log(await err.json());
				return (null);
		}
}
module.exports = {
		getUserInfo,
		getUserInfoFlow
}