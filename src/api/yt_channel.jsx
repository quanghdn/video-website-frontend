var Fetch = require('../functions/fetch');

module.exports = {
	getChannels: function () {
		return Fetch.get('/server/ytchannel');
	},
	getNextUploadedByChannel: function (id, offset, limit) {
		return Fetch.get('/server/ytchannel/' + id + '/uploaded/' + offset + '/' + limit);
	},
	getAllVideoYTIDByChannel: function (id) {
		return Fetch.get('/server/ytchannel/' + id);
	}
};
