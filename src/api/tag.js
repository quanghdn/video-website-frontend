import Config from '../config';
import {METHOD} from '../functions/api';
export default {
		getTagList : () => [METHOD.GET, '/tag']
};