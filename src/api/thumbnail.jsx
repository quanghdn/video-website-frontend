import Config from '../config';
import {METHOD} from '../functions/api';
module.exports = {
		getByID: (idList) => [
				METHOD.GET, '/thumbnail/list/' + idList
		],
		upload: (data) => [METHOD.POSTIMAGE, '/thumbnail/upload', data]
};