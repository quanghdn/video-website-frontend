var Fetch = require('../functions/fetch');

module.exports = {
	addWatchLater: function (id) {
		return Fetch.post('/server/watchlater', { id: id });
	},
	getWatchLater: function () {
		return Fetch.get('/server/watchlater/');
	}
};