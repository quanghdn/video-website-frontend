var Fetch = require('../functions/fetch');

module.exports =  {
	postVideo: function (data) {
		return Fetch.post('/server/video', data);
	},
	getVideo: function (id) {
		return Fetch.get('/server/video/' + id);
	},
	getAllVideoListByCategory: function (categoryID) {
		return Fetch.get('/server/video/category/' + categoryID);
	},
	getVideoListByID: function (listID) {
		return Fetch.get('/server/video/list/' + listID);
	},
	getNextNewVideoListByCategory: function (categoryID, offset, limit, except) {
		return Fetch.get('/server/video/category/' + categoryID + '/new/' + offset + '/' + limit + '/' + except);
	},
	getNextTopVideoListByCategory: function (categoryID, offset, limit, except) {
		return Fetch.get('/server/video/category/' + categoryID + '/top/' + offset + '/' + limit + '/' + except);
	},
	getNextPopularVideoListByCategory: function (categoryID, offset, limit, except) {
		return Fetch.get('/server/video/category/' + categoryID + '/popular/' + offset + '/' + limit + '/' + except);
	},
	getNextNewVideoList: function (offset, limit, except) {
		return Fetch.get('/server/video/new/' + offset + '/' + limit + '/' + except);
	},
	getNextTopVideoList: function (offset, limit, except) {
		return Fetch.get('/server/video/top/' + offset + '/' + limit + '/' + except);
	},
	getNextPopularVideoList: function (offset, limit, except) {
		return Fetch.get('/server/video/popular/' + offset + '/' + limit + '/' + except);
	},
	putVideoView: function (id) {
		return Fetch.put('/server/video/view/' + id, {});
	},
	putVideoLike: function (data) {
		return Fetch.put('/server/video/like', data);
	},
	putVideoDislike: function (data) {
		return Fetch.put('/server/video/dislike', data);
	},
	putVideoNolike: function (data) {
		return Fetch.put('/server/video/nolike', data);
	}
};
