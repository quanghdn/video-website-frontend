import React, {Component} from 'react'
import Slider from '../../components/Slider';
import {VideoItem, SliderItem} from '../../views/'
import TagLabelList from '../TagLabelList';
import style from './style.css';
import loader from '../../functions/loader';
import {Container, Row, Col} from 'reactstrap'
let Home = (props) => {
		let tags = ['tag', 'tag', 'tag', 'tag', 'tag'];
		var settings = {
				dots: true,
				infinite: true,
				speed: 500,
				slidesToShow: 1,
				draggable: false,
				slidesToScroll: 1
		};
		return (
				<Col>
						<Container>
								<Row>
										<Col>
												<TagLabelList tags={tags}/>
										</Col>
								</Row>
								<Row>
										<Col>
												<Slider customName="MainSlider" maxSlides="1" minSlide="1" slideWidth="1000">
														<SliderItem
																src="http://placehold.it/480x360"
																title="Video Title"
																alt="Image Alt"
																description="Video Description"/>
														<SliderItem
																src="http://placehold.it/480x360"
																title="Video Title"
																alt="Image Alt"
																description="Video Description"/>
														<SliderItem
																src="http://placehold.it/480x360"
																title="Video Title"
																alt="Image Alt"
																description="Video Description"/>
												</Slider>
										</Col>
								</Row>
								<Row>
										<h3>Popular Videos</h3>
								</Row>
								<Row>
										<Slider customName="popularVideo" minSlides="1" maxSlides="4" slideWidth="240">
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
										</Slider>
								</Row>
								<Row>
										<h3>Recent Videos</h3>
								</Row>
								<Row>
										<Slider customName="recentVideo" minSlides="4" maxSlides="4">
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
												<VideoItem></VideoItem>
										</Slider>
								</Row>
						</Container>
				</Col>
		)
};
export default Home