import React, {Component} from 'react'
import {Link} from 'react-router'
import style from './style.css';
import {Col, Media, Button} from 'reactstrap';
class VideoItem extends Component {
		render() {
				return (
						<Col>
								<Media>
										<Media
												object
												width="100%"
												src="http://placehold.it/480x360"
												alt="Generic placeholder image"/>
								</Media>
								<strong className={style.time}>10:45</strong>
								<div className={style.info}>
										<div>
												<strong>Video Name</strong>
										</div>
										<div>
												<small>Video Description | 09/2/2017</small>
										</div>
								</div>
						</Col>
				)
		}
}

export default VideoItem