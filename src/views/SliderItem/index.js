import React, {Component} from 'react'
import {Col,Row} from 'reactstrap'
class SliderItem extends Component {
		render() {
				return (
						<li>
								<Row>
										<Col><img src={this.props.src} alt={this.props.alt}/></Col>
										<Col>
												<h1>{this.props.title}</h1>
												<p>
														<em>{this.props.description}</em>
												</p>
										</Col>
								</Row>
						</li>
				)
		}
}

export default SliderItem