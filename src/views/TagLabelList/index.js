import React, {Component} from 'react'
import load from '../../functions/loader';
import VideoItem from '../../views/VideoItem'
import style from './style.css'
let [Badge,Container, Row, Col] = load.components(['Badge','Container', 'Row', 'Col']);
let TagLabelList = (props) => {
		return (
				<Container>
						<Row>
								<Col xs="12" className="text-center">
												{props
														.tags
														.map(tag => <Badge color="info" className={style.tag}>
																<span>{tag}</span>
														</Badge>)}
								</Col>
						</Row>
				</Container>
		)
}
export default TagLabelList