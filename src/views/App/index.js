import React, {Component} from 'react'
import {Link} from 'react-router'
import style from './style.css';
import brand from '../../assets/images/brand.png'
import {Container,Row} from 'reactstrap'
import {Navbar} from '../'
class App extends Component {
		constructor(props) {
				super(props);
		}
		render() {
				return (
						<div>
								<div className={style.container}>
										<Navbar/>
								</div>
								<div>
										<Container>
												<Row>
														{this.props.children}
												</Row>
										</Container>
								</div>
						</div>
				)
		}
}

export default App