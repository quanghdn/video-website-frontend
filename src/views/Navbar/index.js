import React, {Component} from 'react'
import {Link} from 'react-router';
import NavItem from '../../components/NavItem';
import loader from '../../functions/loader';
import {Collapse,Navbar,NavbarToggler,NavbarBrand,Nav} from 'reactstrap';
import {Form,InputGroup,InputGroupAddon,Input} from 'reactstrap';
class NavBarView extends Component {
		constructor(props) {
				super(props);
				this.toggle = this
						.toggle
						.bind(this);
				this.state = {
						isOpen: false
				};
		}
		toggle() {
				this.setState({
						isOpen: !this.state.isOpen
				});
		}
		render() {
				return (
						<Navbar inverse toggleable className="bg-inverse">
								<NavbarToggler right onClick={this.toggle}/>
								<NavbarBrand href="/">ZenQuiz</NavbarBrand>
								<Collapse isOpen={this.state.isOpen} navbar>
										<Nav navbar className="mr-auto">
												<NavItem.Index href="/">Home</NavItem.Index>
												<NavItem href="/playlist">Playlist</NavItem>
												<NavItem href="/upload">Upload</NavItem>
										</Nav>
										<Form className="form-inline">
												<InputGroup>
														<Input placeholder="Find a video..."/>
												</InputGroup>
										</Form>
								</Collapse>
						</Navbar>
				)
		}
}
export default NavBarView