let loader = {
		component: (componentName) => {
				try {
						return require(`../components/${componentName}`).default;
				} catch (err) {
						return require(`reactstrap/lib/${componentName}`).default;
				}
		},
		components: (componentNameArray) => {
				return componentNameArray.map(componentName => {
						return loader.component(componentName);
				});
		},
		view: (viewName) => {
				return require(`../views/${viewName}/index`).default;
		},
		views: (viewNameArray) => {
				return viewNameArray.map(viewName => {
						return loader.view(viewName);
				});
		},
		page: (pageName) => {
				return require(`../pages/${pageName}/index`).default;
		},
		pages: (pageNameArray) => {
				return pageNameArray.map(pageName => {
						return loader.page(pageName);
				});
		},
		store: (storeName) => {
				return require(`../stores/${storeName}`).default;
		},
		stores: (storeNameArray) => {
				return storeNameArray.map(storeName => {
						return loader.store(storeName);
				});
		},
		fn: (fnName) => {
				return require(`../functions/${fnName}`).default;
		},
		stores: (fnNameArray) => {
				return fnNameArray.map(fnName => {
						return loader.fn(fnName);
				});
		}
}
export default loader;