import React from 'react'
import loader from './loader'
let loaderAnnotation = (target)=>{
				return (props)=>{
					let newProps = {
						loader: loader
					};
					Object.assign(newProps,props);
					return React.createElement(target,newProps);
		}
}
export default loaderAnnotation;