let $ = window.jQuery;
let getFileExtension = (path) => {
		return path
				.split('.')
				.pop();
}
let loadCSS = (path) => {
		$('head').append(`<link rel="stylesheet" href="${path}" type="text/css" />`);
}
let loadJS = (path) => {
		let promise = new Promise((resolve, reject) => {
				let el = document.createElement('script');
				$('body').append(el);
				el.onload = function () {
						resolve();
				};
				el.src = path;
		});
		return promise;
}
let load = async(externalArray) => {
		let promises = [];
		externalArray.map(path => {
				switch (getFileExtension(path)) {
						case 'js':
								promises.push(loadJS(path));
								break;
						case 'css':
								loadCSS(path);
								break;
				}
		});
		//return a promise that resolve when all external JS is loaded
		return Promise.all(promises);
}
export default load;