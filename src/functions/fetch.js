var Fetch = require('whatwg-fetch');

module.exports = {
    get: function (url) {
        return new Promise((resolve, reject) => {
            fetch(url, {
                credentials: 'include'
            }).then((response) => {
                if (response.ok === false)
                    return reject(response);
                return resolve(response.json());
            })
        })
    },
    post: function (url, data) {
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        var options = {
            method: 'POST',
            headers: myHeaders,
            mode: 'cors',
            credentials: 'include',
            cache: 'default',
            body: JSON.stringify(data)
        };
        return new Promise((resolve, reject) => {
            fetch(url, options).then((response) => {
                if (response.ok === false)
                    return reject(response);
                return resolve(response.json());
            })
        })
    },
    postImage: function (url, data) {
        var myHeaders = new Headers();
        return fetch(url, {
            method: 'POST',
            headers: myHeaders,
            mode: 'cors',
            credentials: 'include',
            cache: 'default',
            body: data
        })
    },
    put: function (url, data) {
        var myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        var options = {
            method: 'PUT',
            headers: myHeaders,
            mode: 'cors',
            credentials: 'include',
            cache: 'default',
            body: JSON.stringify(data)
        };
        return new Promise((resolve, reject) => {
            fetch(url, options).then((response) => {
                if (response.ok === false)
                    return reject(response);
                return resolve(response.json());
            }).catch(err => console.log(err));
        })
    }
};