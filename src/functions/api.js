import fetch from './fetch';
import Config from '../config';
let METHOD = {
		GET: Symbol('GET'),
		POST: Symbol('POST'),
		PUT: Symbol('PUT'),
		DELETE: Symbol('DELETE'),
		POSTIMAGE: Symbol('POSTIMAGE')
}
function api([method, endpoint, data]) {
		let fn = "";
		switch (method) {
				case METHOD.GET:
						fn = "get"
						break;
				case METHOD.POST:
						fn = "post"
						break;
				case METHOD.PUT:
						fn = "put"
						break;
				case METHOD.DELETE:
						fn = "delete";
						break;
				case METHOD.POSTIMAGE:
						fn = "postImage";
						break;
				default:
						fn = "get";
						break;
		}
		return fetch[fn](Config.server_url + endpoint, data);
}
export {METHOD, api};