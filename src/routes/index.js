import React from 'react';
import {Route, IndexRoute, IndexRedirect} from 'react-router'
import load from '../functions/loader';
let [AppPage,
		HomePage,
		VideoPage,
		CategoryPage,
		VideoUploadPage,
		AdminPage] = load.pages([
		'AppPage',
		'HomePage',
		'VideoPage',
		'CategoryPage',
		'VideoUploadPage',
		'AdminPage'
]);
export default(
		<Route name="root" path='/'>
				<Route name="app" component={AppPage}>
						<IndexRoute component={HomePage}/>
						<Route path='upload' component={VideoPage}></Route>
						<Route path='upload/:id' component={VideoPage}></Route>
						<Route path='category'>
								<Route path=":id" component={CategoryPage}></Route>
						</Route>
						<Route path='video'>
								<Route path=":id" component={VideoPage}></Route>
						</Route>
				</Route>
				<Route name="admin" path='/admin' component={AdminPage}></Route>
		</Route>
)