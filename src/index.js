import React from 'react';
import ReactDOM from 'react-dom';
import 'tether';
import * as ReactRouter from 'react-router'
import routes from './routes'
import externalLoader from './functions/external-loader'
import externalArray from './external/'
import './index.css'
//Load external first
console.log(externalArray);
externalLoader(externalArray)
//Load Router
		.then(() => {
		console.log("External Script Loaded");
		ReactRouter.match({
				history: ReactRouter.browserHistory,
				routes: routes
		}, function (error, redirectLocation, renderProps) {
				ReactDOM.render(
						<ReactRouter.Router history={ReactRouter.browserHistory} {...renderProps}/>, document.getElementById('root'));
		});
		console.log("Router Loaded");
})
