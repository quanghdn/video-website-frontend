import React from 'react'
import {Link,IndexLink} from 'react-router'
let NavItem = (props) => {
		return (
				<li className="nav-item">
						<Link to={props.href} activeClassName="active" className={`nav-link`}>
								{props.children}
						</Link>
				</li>
		)
}
NavItem.Index = (props) => {
		return (
				<li className="nav-item">
						<IndexLink to={props.href} activeClassName="active" className={`nav-link`}>
								{props.children}
						</IndexLink>
				</li>
		)
}
export default NavItem