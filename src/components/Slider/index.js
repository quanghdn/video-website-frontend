import React, {Component} from 'react'
let $ = window.jQuery;
import style from './style.css'
class Slider extends Component {
		componentDidMount() {
			console.log("minSlides: " +this.props.minSlides);
				$('.' + this.props.customName).bxSlider({minSlides: this.props.minSlides, maxSlides: this.props.maxSlides,slideWidth : this.props.slideWidth, slideMargin: 10});
		}
		render() {
				return (
						<ul className={this.props.customName}>
								{this.props.children}
						</ul>
				)
		}
}
export default Slider