module.exports = {
	"extends": "eslint:recommended",
	"plugins": [
		"standard",
		"react"
	],
	"env": {
		"browser": true,
		"node": true,
		"es6": true
	},
	"parser": "babel-eslint",
	"parserOptions": {
		"ecmaFeatures": {
			"jsx": true
		}
	},
	"globals": {
		"__base": true,
		"__config": true,
		"React": true,
		"ReactRouter": true,
		"ReactDOM": true
	},
	"rules": {
		"react/jsx-uses-react": "error",
		"react/jsx-uses-vars": "error",
		"no-console": 0
	}
};